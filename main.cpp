/*
 * main.cpp
 *
 *  Created on: 20 дек. 2017 г.
 *      Author: root
 */

#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <numeric>
#include <cmath>

#include "netcalc.h"

//Генерация рандомного массива
template<typename T>
std::vector<T> test_generateRandomDouble(size_t _count, T _minValue, T _maxValue)
{
	std::vector<T> retRandomArray;
	srand(time(NULL));

	for(size_t ix = 0; ix < _count; ix++)
	{
		T f = (T)rand() / RAND_MAX;
		retRandomArray.push_back(_minValue + f * (_maxValue - _minValue));
	}
	return retRandomArray;
}

//Чтение данных из файла
template<typename T>
std::vector<T> readDataArrayFromFile(std::string _filename)
{
	std::vector<T> retArray;
	std::ifstream input(_filename);
	if (input.is_open()) {
	    T number;

	    while (input >> number)
	    	retArray.push_back(number);
	}
	input.close();

	return retArray;
}

int main(int argc, char *argv[])
{
	//std::stringstream filename;
	netcalc::netcalc<long double> networkMeasurement;
	//std::vector<double> inputData = readDataArrayFromFile<double>(filename.str());
	std::vector<long double> inputData = test_generateRandomDouble<long double>(178, 1, 100);


	networkMeasurement.calculateNetParameters(inputData);//netcalc::transformArrayToLogArray<long double>(inputData));

	std::cout << "Hurst for RS method is: " << networkMeasurement.getHurst(0x01) << ";\n";
	std::cout << "Hurst for Haar method is: " << networkMeasurement.getHurst(0x02) << ";\n";
	//std::cout << "3: " << networkMeasurement.calculateHurstByDaubechiesD4Wavelets(inputData) << ";\n";
	std::cout << "Jitter for calc is: " << networkMeasurement.getJitter() << ";\n";

	return 0;
};


