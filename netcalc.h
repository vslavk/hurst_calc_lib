/*
Copyright December 2017 Kulik Viacheslav

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Description
Header only library for calculation Hurst and Jitter parameters.
*/

#ifndef NETCALC_H_
#define NETCALC_H_

#include <vector>
#include <numeric>
#include <cmath>
#include <algorithm>
#include <iostream>

namespace netcalc
{

//This class describes different calculation methods of Hurst and Jitter parameters
template<typename T>
class netcalc {
private:
	long double jitter;
	size_t defaultHurstType;
	long double rsHurst;
	long double wsdHaarHurst;

public:
	netcalc() : jitter(0), rsHurst(0), wsdHaarHurst(0), defaultHurstType(0x01) {};
	~netcalc() {};

	long double getJitter() 
	{ 
		return this->jitter; 
	};

	long double getHurst(size_t _hurstCalculationType = 0x01)
	{
		long double retHurst = -1;
		switch(_hurstCalculationType)
		{
		case 0x01:
			retHurst = this->rsHurst;
			break;
		case 0x02:
			retHurst = this->wsdHaarHurst;
			break;
		default:
			if(defaultHurstType == 0x01)
				retHurst = this->rsHurst;
			else if(defaultHurstType == 0x02)
				retHurst = this->wsdHaarHurst;
			break;
		}
		return retHurst;
	};


	//This methods initialize calculation procedure
	void calculateNetParameters(std::vector<T> _data, size_t _hurstCalculationType = 0xFF)
	{
		this->jitter = calculateJitter(_data);
		defaultHurstType = _hurstCalculationType;

		switch(defaultHurstType)
		{
			case 0x01:
			{
				this->rsHurst = calculateHurstByRSAnalysis(_data);
				break;
			}
			case 0x02:
			{
				this->wsdHaarHurst = calculateHurstByHaarWavelets(_data);
				break;
			}
			default:
			{
				defaultHurstType = 0x01;
				this->rsHurst = calculateHurstByRSAnalysis(_data);
				this->wsdHaarHurst = calculateHurstByHaarWavelets(_data);
				break;
			}
		}
	}

private:
	std::vector<T> transformArrayToLogArray(std::vector<T> _data)
	{
		std::vector<T> LogData;

		for(auto ix = _data.begin(); ix < _data.end()-1; ++ix)
			LogData.push_back(log(*(ix+1) / *ix));

		return LogData;
	}

	//This methods calculate total jitter parameters
	long double calculateJitter(std::vector<T> _data)
	{
		long double retJitter = 0;

		for(auto ix = _data.begin(); ix < (_data.end()-1); ++ix)
			retJitter += (abs(*(ix+1) - *ix));

		return retJitter/_data.size();
	}

	//Method of Least Squares for Hurst calculation
	long double calculateMethodLeastSquares(size_t _size, std::vector<T> _x, std::vector<T> _y)
	{
		long double xsum = 0, x2sum = 0, ysum = 0, xysum = 0;
		for (size_t ix = 0; ix < _x.size(); ix++)
		{
			xsum += _x[ix];
			ysum += _y[ix];
			x2sum += pow(_x[ix],2);
			xysum += _x[ix] * _y[ix];
		}

		long double a = (_size * xysum - xsum * ysum) / (_size * x2sum - xsum * xsum);
		//long double b = (x2sum * ysum - xsum * xysum) / (x2sum * _size - xsum * xsum);
		return a;
	}

	long double RSCalculation(size_t _period, std::vector<T> _data)
	{
		long double RS = 0;
		std::vector<long double> RSi;

		std::cout << _period << std::endl;
		auto dataIter = _data.begin();
		while(dataIter < _data.end())
		{
			std::vector<T> bufData;

			//std::cout << (_data.end() - dataIter) << " + " << _period << " = " << (_data.end() - dataIter) + _period << "; " << (_data.end()-_data.begin()) << std::endl;
			if( ( _data.end() - dataIter) < _period )
				break;
				//std::copy(dataIter, _data.end(), std::back_inserter(bufData));
			else
				std::copy_n(dataIter, _period, std::back_inserter(bufData));

			long double mean = std::accumulate(bufData.begin(), bufData.end(), 0)/bufData.size();

			std::vector<long double> X;
			X.push_back((*bufData.begin()) - mean);
			long double minX = X.back(), maxX = X.back();
			for(auto ix = bufData.begin() + 1; ix < bufData.end(); ++ix)
			{
				X.push_back(X.back() + (*ix) - mean);
				if(maxX < X.back()) maxX = X.back();
				if(minX > X.back()) minX = X.back();
			}

			long double Ri = maxX - minX;


			long double Si = 0;
			for(auto ix = bufData.begin(); ix < bufData.end(); ++ix)
				Si = Si + pow(((*ix) - mean), 2);

			Si = std::sqrt(Si/bufData.size());

			RSi.push_back(Ri/Si);

			std::cout << Ri  << " " << Si << " = " << RSi.back() << std::endl;
			dataIter = dataIter + _period;
			if(static_cast<long int>((_data.end() - dataIter) + _period) > (_data.end()-_data.begin()))
				dataIter = _data.end();
		}

		RS = std::accumulate(RSi.begin(), RSi.end(), 0.0)/RSi.size();

		return RS;
	}

	//Hurst calculation by using RS analysis method
	long double calculateHurstByRSAnalysis(std::vector<T> _data)
	{
		long double Hurst = -1;
		std::vector<T> RS, Periods;
	/*
		std::vector<T> analyzeData;
		for(auto ix = _data.begin(); ix < _data.end()-1; ix+=2)
		{
			analyzeData.push_back( std::log10(*(ix) / *(ix+1)) );
		}
	*/
		for(size_t ix = 3; ix < _data.size()/2; ++ix)
		{
			RS.push_back(std::log10(RSCalculation(ix, _data)));
			Periods.push_back(std::log10(ix));
		}

		Hurst = calculateMethodLeastSquares(Periods.size(), Periods, RS);

		return Hurst;
	}

	//Recursive method for Haar Calculation
	std::vector<T> calculateApproximatesDetails(std::vector<T> _data)
	{
		std::vector<T> approximates, details;
		if(_data.size() != 1)
		{
			for(auto ix = _data.begin(); ix < _data.end(); ix = ix + 2)
			{
				approximates.push_back((*ix + *(ix+1))/2);
				details.push_back((*ix - *(ix+1))/2);
			}
			approximates = calculateApproximatesDetails(approximates);
		}
		else approximates = _data;
		approximates.insert(approximates.end(),details.begin(), details.end());
		return approximates;
	}

	//Hurst calculation by using Haar Wavelet Spectral Density method
	long double calculateHurstByHaarWavelets(std::vector<T> _data)
	{
		long double Hurst = -1;
		std::vector<T> correctData, period;

		size_t correctSize = pow( 2, static_cast<size_t>( log2( _data.size() ) ) );
		std::copy_n(_data.begin(), correctSize, std::back_inserter(correctData));

		correctData = calculateApproximatesDetails(correctData);
		while(correctSize > 1)
		{
			period.push_back(correctSize);
			correctSize = correctSize/2;
		}

		period.push_back(correctSize);
		std::reverse(period.begin(), period.end());
		Hurst = calculateMethodLeastSquares(correctData.size(), period, correctData);

		Hurst = fabs((Hurst-1)/2);

		return Hurst;
	}
};

};
#endif /* NETCALC_H_ */
